-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2018 at 05:51 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ikomers`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` tinyint(2) NOT NULL,
  `username_admin` varchar(35) NOT NULL,
  `fullname_admin` varchar(45) NOT NULL,
  `password_admin` varchar(255) NOT NULL,
  `level_admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `username_admin`, `fullname_admin`, `password_admin`, `level_admin`) VALUES
(1, 'admin', 'Adminstrator', '$2y$10$uIcOWSE1dbAO6k7OJypvku9odk183rFqJHpZLWU4QtN6.DNIdRuVy', 1);

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_item` int(7) NOT NULL,
  `nama_item` varchar(255) NOT NULL,
  `harga_item` int(10) NOT NULL,
  `berat_item` int(5) NOT NULL,
  `jumlah_item` int(3) NOT NULL,
  `status_item` tinyint(1) NOT NULL,
  `gambar_item` varchar(255) NOT NULL,
  `desk_item` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_item`, `nama_item`, `harga_item`, `berat_item`, `jumlah_item`, `status_item`, `gambar_item`, `desk_item`) VALUES
(1, '12 Strong', 150000, 1, 7, 1, 'gambar1544287588.jpg', '12 Strong tells the story of the first Special Forces team deployed to Afghanistan after 9/11; under the leadership of a new captain, the team must work with an Afghan warlord to take down the Taliban.'),
(2, 'Maze Runner: The Death Cure ', 145000, 1, 9, 1, 'gambar1544287664.jpg', 'Young hero Thomas embarks on a mission to find a cure for a deadly disease known as \"The Flare\".'),
(3, 'Black Panther (2018)', 175000, 1, 7, 1, 'gambar1544287708.jpg', 'T\'Challa, heir to the hidden but advanced kingdom of Wakanda, must step forward to lead his people into a new future and must confront a challenger from his country\'s past.'),
(4, 'The 15:17 to Paris ', 125000, 1, 6, 1, 'gambar1544287848.jpg', 'Americans discover a terrorist plot on a Paris-bound train.');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(7) NOT NULL,
  `username` varchar(35) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `password_user` varchar(255) NOT NULL,
  `jk_user` enum('L','P') NOT NULL,
  `telepon_user` varchar(20) NOT NULL,
  `alamat_user` text NOT NULL,
  `status_user` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `nama_lengkap`, `email_user`, `password_user`, `jk_user`, `telepon_user`, `alamat_user`, `status_user`) VALUES
(1, 'abismaw', 'Aria Wahyutama', 'yellowreeds@gmail.com', '$2y$10$mlPidOhPHk/h5YCLr99R2.a0lFJhXVsqrsoE6EIjdUgPB.L/drKAy', 'L', '123456789', 'dimana aja', 1),
(9, 'biasaitw', 'test test', 'test2@test2.com', '$2y$10$UQ9tGkqKjMfqyPlVB0jT1Oe3LTIwUZuoMacPp/cGUa6YVWaSUi7TO', 'L', '987654321', 'tuiikolpfaergqsdgadfs', 1),
(10, 'deadman102', 'test tist', 'deadman@deadman.com', '$2y$10$rxJxuNlz6ZWAxtZ4KToN5.F5FhlzSgdMnUORcVm7116O2QpyJ8bQC', 'L', '12334455667', 'alamat saya di dunia ini', 1),
(14, 'abismaweee', 'Aria Wahyutama', 'yellowreedsssss@gmail.com', '$2y$10$qK3cbyu1AJ2WXfVt1G0bLu7O8GBhc5jhqcW/FzsPmuoDguB1lDj0e', 'L', '11223344556', 'alamat nya juga dimana aja', 1),
(16, 'abismaweeeeee', 'Aria Wahyutama', 'abismawwwww@abismaw.com', '$2y$10$S2ojnRX2b3oqbrDayNxip.PplmvxsS7WHPRQ3cByo9Py2im/YBezq', 'L', '11223344556', 'saya ga tau alamat saya dimana', 1),
(17, 'mccotton123', 'Aaron McCotton', 'aaron@aaron.com', '$2y$10$GkukPqcYV1olaj8OBiOFaOaU1yphnNN0VJpoyjRNIpapRrYdR5z2m', 'L', '112233445', 'alamat saya dimana aja', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`),
  ADD UNIQUE KEY `username_admin` (`username_admin`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_item`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email_user` (`email_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_item` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
