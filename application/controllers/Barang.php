<?php

require APPPATH . '/libraries/REST_Controller.php';

class Barang extends REST_Controller
{

    public function __construct($config = "rest")
    {
        parent::__construct($config);
        $this->load->model("Barang_model", "barang");
    }

    public function index_get()
    {
        $id = $this->get('id_item');
        $cari = $this->get('cari');

        if($cari != "") {
            $barang = $this->barang->cari($cari)->result();
        } else if($id == '') {
            $barang = $this->barang->getData(null)->result();
        } else {
            $barang = $this->barang->getData($id)->result();
        }

        $this->response($barang);
    }

    public function index_put()
    {
        $nama_item = $this->put('nama_item');
        $harga_item = $this->put('harga_item');
        $berat_item = $this->put('berat_item');
        $jumlah_item = $this->put('jumlah_item');
        $status_item = $this->put('status_item');
        $gambar_item = $this->put('gambar_item');
        $desk_item = $this->put('desk_item');
        $id = $this->put('id_item');
        $data = array(
            'nama_item' => $nama_item,
            'harga_item' => $harga_item,
            'berat_item' => $berat_item,
            'status_item' => $status_item,
            'gambar_item' => $gambar_item,
            'desk_item' => $desk_item
        );
        $update = $this->barang->update('barang', $data, 'id_item', $this->put('id_item'));

        if($update) {
            $this->response(array('status' => 'success', 200));        
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_post()
    {
        $nama_item = $this->post('nama_item');
        $harga_item = $this->post('harga_item');
        $berat_item = $this->post('berat_item');
        $jumlah_item = $this->post('jumlah_item');
        $status_item = $this->post('status_item');
        $gambar_item = $this->post('gambar_item');
        $desk_item = $this->post('desk_item');

        $data = array(
            'nama_item' => $nama_item,
            'harga_item' => $harga_item,
            'berat_item' => $berat_item,
            'status_item' => $status_item,
            'gambar_item' => $gambar_item,
            'desk_item' => $desk_item  
        );
        $insert = $this->barang->insert($data);

        if($insert) {
            $this->response(array('status' => 'success', 200));        
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_delete()
    {
       $id = $this->delete('id_item');
       $delete = $this->barang->delete('barang', 'id_item', $id);
       if($delete) {
            $this->response(array('status' => 'success', 200));        
        } else {
            $this->response(array('status' => 'fail', 502));
       }
    }
}
