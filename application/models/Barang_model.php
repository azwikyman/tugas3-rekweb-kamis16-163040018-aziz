<?php

class Barang_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

    public function getData($id = null) {
        $this->db->select('*');
        $this->db->from('barang');

        if($id == null) {
            $this->db->order_by('id_item', 'asc');
        } else {
            $this->db->where('id_item', $id);
        }
        return $this->db->get();
    }

    public function cari($cari) {
        $this->db->select('*');
        $this->db->from('barang');
        $this->db->like('nama_item', $cari);
        $this->db->or_like('harga_item', $cari);
        $this->db->or_like('berat_item', $cari);
        $this->db->or_like('jumlah_item', $cari);
        return $this->db->get();
    }

    public function insert($data){
       $this->db->insert('barang', $data);
       return $this->db->affected_rows();
    }

    public function update($table, $data, $par, $var) {
        $this->db->update($table, $data, array($par => $var));
        return $this->db->affected_rows();
    }
    
    public function delete($table, $par, $var){
       $this->db->where($par, $var);
       $this->db->delete($table);
       return $this->db->affected_rows();
    }
}
